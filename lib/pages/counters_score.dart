import 'package:flutter/material.dart';
import './home.dart';

class CountersScore extends StatefulWidget {
  final String team1, team2;

  CountersScore({Key key, @required this.team1, @required this.team2})
      : super(key: key);
  @override
  _CountersScoreState createState() => _CountersScoreState();
}

class _CountersScoreState extends State<CountersScore> {
  int score1 = 0;
  int score2 = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, 100),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(color: Colors.black12, spreadRadius: 5, blurRadius: 2)
          ]),
          width: MediaQuery.of(context).size.width,
          height: 100,
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [Colors.indigo, Colors.cyan]),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20))),
            child: Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Score Counter",
                    style: TextStyle(fontSize: 30, color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 100,
                height: 100,
                alignment: Alignment.center,
                color: Colors.grey,
                margin: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      score1.toString(),
                      style:
                          TextStyle(fontSize: 80, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                width: 100,
                height: 100,
                color: Colors.grey,
                alignment: Alignment.center,
                margin: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      score2.toString(),
                      style:
                          TextStyle(fontSize: 80, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 100,
                height: 100,
                alignment: Alignment.center,
                margin: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '${widget.team1}',
                      style:
                          TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                width: 100,
                height: 100,
                alignment: Alignment.center,
                margin: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '${widget.team2}',
                      style:
                          TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FlatButton(
                color: Colors.red,
                textColor: Colors.white,
                splashColor: Colors.redAccent,
                height: 80,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                padding: EdgeInsets.all(10),
                onPressed: () {
                  setState(() {
                    score1--;
                  });
                },
                child: Text(
                  "-",
                  style: TextStyle(fontSize: 40),
                ),
              ),
              SizedBox(
                width: 20,
              ),
              FlatButton(
                color: Colors.red,
                textColor: Colors.white,
                splashColor: Colors.redAccent,
                height: 80,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                padding: EdgeInsets.all(10),
                onPressed: () {
                  setState(() {
                    score2--;
                  });
                },
                child: Text(
                  "-",
                  style: TextStyle(fontSize: 40),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FlatButton(
                  color: Colors.green,
                  textColor: Colors.white,
                  splashColor: Colors.greenAccent,
                  height: 80,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  padding: EdgeInsets.all(10),
                  onPressed: () {
                    setState(() {
                      score1++;
                    });
                  },
                  child: Text(
                    "+",
                    style: TextStyle(fontSize: 40),
                  )),
              SizedBox(
                width: 20,
              ),
              FlatButton(
                color: Colors.green,
                textColor: Colors.white,
                splashColor: Colors.greenAccent,
                height: 80,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                padding: EdgeInsets.all(10),
                onPressed: () {
                  setState(() {
                    score2++;
                  });
                },
                child: Text(
                  "+",
                  style: TextStyle(fontSize: 40),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            FlatButton(
              color: Colors.indigo,
              textColor: Colors.white,
              splashColor: Colors.indigoAccent,
              height: 50,
              padding: EdgeInsets.symmetric(horizontal: 60),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Home()));
                setState(() {});
              },
              child: Text(
                "Reset",
                style: TextStyle(fontSize: 30),
              ),
            ),
          ]),
        ],
      )),
    );
  }
}
